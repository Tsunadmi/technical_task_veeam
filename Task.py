import datetime
import time
import pandas as pd
import psutil
import csv
import matplotlib.pyplot as plt


def creating_table():  # this function creates csv file with fieldnames
    with open('my.csv', 'w', newline='', encoding='utf-8') as f:
        fieldnames = ['Cpu_usage(%)', 'Memory_usage(%)', 'Number_handles(pcs)', 'Time']
        twit = csv.DictWriter(f, fieldnames=fieldnames)
        twit.writeheader()
        twit.writerow({'Cpu_usage(%)': None, 'Memory_usage(%)': None, 'Number_handles(pcs)': None, 'Time': None})
        f.close()


def task(cpu_usage, memory_usage, number_handles, bars):  # this function does all endpoints
    result = []
    for proc in number_handles:
        try:
            result.append(
                proc.open_files())  # Endpoint 3 --> Number of open handles
        except:
            pass
    cpu_percent = cpu_usage / 100.0  # Endpoint 1 --> CPU usage
    cpu_bar = '█' * int(cpu_percent * bars) + '-' * (bars - int(cpu_percent * bars))  # terminals information
    memory_percent = memory_usage / 100.0  # Endpoin 2 --> Memory consumption
    memory_bar = '█' * int(memory_percent * bars) + '-' * (
            bars - int(memory_percent * bars))  # terminals information
    data_for_adding = [cpu_usage, memory_usage, len(result), datetime.datetime.now()]  # adding all data to csv file
    with open('my.csv', 'a', newline='', encoding='utf-8') as f:  # adding all data to csv file
        twit = csv.writer(f)
        twit.writerow(data_for_adding)
        f.close()
    print(f'\rCPU Usage: |{cpu_bar}| {cpu_usage: .2f}%', end=' ')  # terminals information
    print(f'Memory Usage: |{memory_bar}| {memory_usage: .2f}%', end='')  # terminals information


def digit_check():  # # this function checks time
    while True:
        digit = input('Enter number, how many seconds process should go\n'
                      '!remark! it is recommended to set more then 30 sec! : ')
        if digit.isdigit():
            print('Start of the process:')
            return digit
        9
def graf(): # this function create graf
    plt.style.use('fivethirtyeight')
    df = pd.read_csv('my.csv')
    x = df['Number_handles(pcs)']
    y = df['Cpu_usage(%)']
    plt.xlabel('Number Handles(pcs)', fontsize=18)
    plt.ylabel('Cpu Usage(%)',fontsize=16)
    plt.bar(x, y)
    plt.show()

if __name__ == "__main__":  # main program.
    # creating_table() # creating table in csv file
    # graf() # drawing of charts
    timeout = time.time() + int(digit_check())  # progress time
    while True:
        if time.time() > timeout:
            break
        task(psutil.cpu_percent(), psutil.virtual_memory().percent, psutil.process_iter(), 30)
        time.sleep(0.5)
