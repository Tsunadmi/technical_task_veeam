# Technical_Task_Veeam

Problem 1
Implement a program that will launch a specified process and periodically (with a provided time
interval) collect the following data about it:
- CPU usage (percent);
- Memory consumption: Working Set and Private Bytes (for Windows systems) or Resident
Set Size and Virtual Memory Size (for Linux systems);
- Number of open handles (for Windows systems) or file descriptors (for Linux systems).
Data collection should be performed all the time the process is running. Path to the executable
file for the process and time interval between data collection iterations should be provided by
user. Collected data should be stored on the disk. Format of stored data should support
automated parsing to potentially allow, for example, drawing of charts.

## Programming languages:

python 3.9:

## Virtual environment

You should activate virtual environment in terminal in project folder:

```
venv/Scripts/activate
```

## Add important libraries

```
import datetime
import time
import pandas
import psutil
import csv
import matplotlib.pyplot

```
All detailed  information you can find in **requirements.txt**

## Getting started

Start of app: **Task.py**
```
python Task.py
```

## Description

The first step is creating csv file **my.csv** with fieldnames. --> ```def creating_table()```

The second step is to find all data which we need. I useed **library psutil** for this point

This function --> ```def task(cpu_usage, memory_usage, number_handles, bars)```

find necessary information and save it in csv file.

```
Cpu_usage(%),Memory_usage(%),Number_handles(pcs),Time
10.3,63.3,135,2022-08-03 22:06:06.802895
24.0,63.6,136,2022-08-03 22:06:37.126878
24.2,63.4,134,2022-08-03 22:07:03.831330
28.2,63.6,135,2022-08-03 22:07:34.422506
23.1,63.4,134,2022-08-03 22:08:06.142222
26.6,63.4,133,2022-08-03 22:08:34.010828
28.7,64.9,134,2022-08-03 22:09:04.074762
25.0,64.9,135,2022-08-03 22:09:33.208154
24.5,64.8,134,2022-08-03 22:10:00.562096
30.6,64.1,134,2022-08-03 22:10:26.440912
30.4,63.7,134,2022-08-03 22:10:56.866223
26.5,63.8,133,2022-08-03 22:11:27.033706
28.1,63.7,133,2022-08-03 22:11:55.203737
27.9,64.0,134,2022-08-03 22:12:22.873278
28.9,62.9,133,2022-08-03 22:12:59.944476
18.3,63.9,134,2022-08-03 22:13:27.698939
25.3,64.1,134,2022-08-03 22:13:57.439767
26.4,63.1,134,2022-08-03 22:14:29.866545
23.4,63.2,136,2022-08-03 22:14:59.953687
25.2,62.9,136,2022-08-03 22:15:29.990859
```

However cycle for finding and adding first two Endpoint is quick, for last Endpoint it takes approximately 25-30 sec per cycle.
This happens because the library searches absolutely all the files and folders.

Also this code  
```cpu_bar = '█' * int(cpu_percent * bars) + '-' * (bars - int(cpu_percent * bars))```  
and  
```memory_bar = '█' * int(memory_percent * bars) + '-' * (bars - int(memory_percent * bars))``` 
draws a small animation in terminal.

CPU Usage: |█████-------------------------|  19.20% Memory Usage: |████████████████████----------|  67.10%

CPU Usage: |██████------------------------|  20.50% Memory Usage: |████████████████████----------|  67.20%

CPU Usage: |██████------------------------|  21.60% Memory Usage: |████████████████████----------|  67.30%

CPU Usage: |█████-------------------------|  18.80% Memory Usage: |████████████████████----------|  68.00%

Then this function --> ```def digit_check()```requests time for user and also check input data.
```
digit = input('Enter number, how many seconds process should go: ')
```
I recommend to set more then 30 sec as explained above.

With the help this function ```def graf():``` i can create a graph with data from csv file.


In main function was used endless cycle. Exit from this cycle was provide with time which users set. ```timeout = time.time() + int(digit_check())```
```
if __name__ == "__main__":
   while True:
        if time.time() > timeout:
            break
        task(psutil.cpu_percent(), psutil.virtual_memory().percent, psutil.process_iter(), 30)
        time.sleep(0.5)

```

## Author

This app was done by Dmitry Tsunaev.

- [ ] [LinkedIn](http://linkedin.com/in/dmitry-tsunaev-530006aa)



