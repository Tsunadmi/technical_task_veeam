import time
import psutil


def task(cpu_usage, memory_usage, bars):
    cpu_percent = cpu_usage / 100.0
    cpu_bar = '█' * int(cpu_percent * bars) + '-' * (bars - int(cpu_percent * bars))
    memory_percent = memory_usage / 100.0
    memory_bar = '█' * int(memory_percent * bars) + '-' * (bars - int(memory_percent * bars))
    print(f'\rCPU Usage: |{cpu_bar}| {cpu_usage: .2f}%', end=' ')
    print(f'Memory Usage: |{memory_bar}| {memory_usage: .2f}%', end='')


def digit_check():
    while True:
        digit = input('Enter number, how many seconds process should go: ')
        if digit.isdigit():
            return digit


if __name__ == "__main__":

    timeout = time.time() + int(digit_check())
    while True:
        if time.time() > timeout:
            break
        task(psutil.cpu_percent(), psutil.virtual_memory().percent, 30)
        time.sleep(0.3)
